'use strict';

(function ($) {
  'use strict';

  // Infoblock charts

  new Chart($('#infoblock-chart-1'), {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [45, 40, 30, 20, 25, 35, 50],
        fill: true,
        backgroundColor: '#e53935',
        borderColor: '#e53935',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#e53935',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#e53935',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  new Chart($('#infoblock-chart-2'), {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [30, 22, 18, 25, 40, 55, 60],
        fill: true,
        backgroundColor: '#7d57c1',
        borderColor: '#7d57c1',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#7d57c1',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#7d57c1',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  // Flot chart
  var data1 = [[1, 10], [2, 5], [3, 12], [4, 6], [5, 10], [6, 7], [7, 15]];
  var data2 = [[1, 6], [2, 3], [3, 7], [4, 4], [5, 8], [6, 5], [7, 10]];

  var label1 = 'Page views';
  var label2 = 'Sales';

  var color1 = '#1d87e4';
  var color2 = tinycolor('#1d87e4').darken(15).toString();

  $.plot($('.flot-chart-example'), [{
    data: data1,
    label: label1,
    color: color1
  }, {
    data: data2,
    label: label2,
    color: color2
  }], {
    series: {
      lines: {
        show: true,
        fill: true,
        lineWidth: 1,
        fillColor: {
          colors: [{
            opacity: 1
          }, {
            opacity: 1
          }]
        }
      },
      points: {
        show: true,
        radius: 0
      },
      shadowSize: 0,
      curvedLines: {
        apply: true,
        active: true,
        monotonicFit: true
      }
    },
    grid: {
      margin: {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
      },
      color: '#aaa',
      hoverable: true,
      borderWidth: 0,
      backgroundColor: '#fff',
      labelMargin: 0,
      minBorderMargin: 0,
      axisMargin: 0
    },
    legend: {
      show: false
    },
    xaxis: {
      show: false
    },
    yaxis: {
      ticks: 0
    },
    tooltip: true,
    tooltipOpts: {
      content: '%s: %y',
      shifts: {
        x: -60,
        y: 25
      },
      defaultTheme: false
    }
  });

  /* Line Chart */
  var data3 = [[1, 10], [2, 20], [3, 12], [4, 28], [5, 15]];

  var labels = ["Tickets"];
  var colors = ['#34a853'];

  $.plot($("#chart-1"), [{
    data: data3,
    label: labels[0],
    color: colors[0]
  }], {
    series: {
      lines: {
        show: true,
        fill: true,
        lineWidth: 3,
        fillColor: {
          colors: [{
            opacity: 1
          }, {
            opacity: 1
          }]
        }
      },
      points: {
        show: true,
        radius: 0
      },
      shadowSize: 0,
      curvedLines: {
        apply: true,
        active: true,
        monotonicFit: true
      }
    },
    grid: {
      labelMargin: 10,
      color: "#aaa",
      hoverable: true,
      borderWidth: 0,
      backgroundColor: "#fff"
    },
    legend: {
      show: false
    },
    tooltip: true,
    tooltipOpts: {
      content: '%s: %y',
      shifts: {
        x: -60,
        y: 25
      },
      defaultTheme: false
    }
  });

  /* Bar chart */
  var data4 = [];
  for (var i = 0; i <= 6; i += 1) {
    data4.push([i, parseInt(Math.random() * 20)]);
  }

  var data5 = [];
  for (var o = 0; o <= 6; o += 1) {
    data5.push([o, parseInt(Math.random() * 20)]);
  }

  var data6 = [{
    label: "Data One",
    data: data4,
    bars: {
      order: 1
    }
  }, {
    label: "Data Two",
    data: data5,
    bars: {
      order: 2
    }
  }];

  $.plot($("#chart-2"), data6, {
    bars: {
      show: true,
      barWidth: 0.2,
      fill: 1
    },
    series: {
      stack: 0
    },
    grid: {
      color: "#aaa",
      hoverable: true,
      borderWidth: 0,
      labelMargin: 5,
      backgroundColor: "#fff"
    },
    legend: {
      show: false
    },
    colors: ["#faa800", "#e53935"],
    tooltip: true, //activate tooltip
    tooltipOpts: {
      content: "%s : %y.0",
      shifts: {
        x: -30,
        y: -50
      }
    }
  });

  /* Donut chart */
  var data7 = [{
    label: "Mobile",
    data: 3,
    color: "#1d87e4"
  }, {
    label: "Tablet",
    data: 4,
    color: "#7d57c1"
  }, {
    label: "Desktop",
    data: 6,
    color: "#e53935"
  }];

  $.plot($("#chart-3"), data7, {
    series: {
      pie: {
        innerRadius: 0.5,
        show: true
      }
    },
    grid: {
      hoverable: true
    },
    legend: {
      show: false
    },
    color: null,
    tooltip: true,
    tooltipOpts: {
      content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
      shifts: {
        x: 20,
        y: 0
      },
      defaultTheme: false
    }
  });

  // Realtime chart
  $(function () {

    // We use an inline data source in the example, usually data would
    // be fetched from a server

    var data = [],
        totalPoints = 300;

    function getRandomData() {
      if (data.length > 0) {
        data = data.slice(1);
      }

      // Do a random walk

      while (data.length < totalPoints) {
        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;

        if (y < 5) {
          y = 5;
        } else if (y > 95) {
          y = 95;
        }

        data.push(y);
      }

      // Zip the generated y values with the x values

      var res = [];
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
      }

      return res;
    }

    // Set up the control widget

    var updateInterval = 60;

    var plot = $.plot("#realtime", [getRandomData()], {
      series: {
        shadowSize: 0 // Drawing is faster without shadows
      },
      yaxis: {
        min: 0,
        max: 100
      },
      xaxis: {
        min: 0,
        max: 300
      },
      colors: ["#34a853"],
      grid: {
        color: "#aaa",
        hoverable: true,
        borderWidth: 0,
        backgroundColor: '#fff'
      },
      tooltip: true,
      tooltipOpts: {
        content: "Y: %y",
        defaultTheme: false
      }
    });

    function update() {
      plot.setData([getRandomData()]);

      // Since the axes don't change, we don't need to call plot.setupGrid()

      plot.draw();
      setTimeout(update, updateInterval);
    }

    update();
  });
})(jQuery);

'use strict';

(function ($) {
  'use strict';

  // Tiles charts

  new Chart($('#tile-chart-1'), {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [30, 22, 18, 25, 40, 55, 60],
        fill: true,
        backgroundColor: '#1d87e4',
        borderColor: '#1d87e4',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#1d87e4',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#1d87e4',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  new Chart($('#tile-chart-2'), {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [50, 45, 30, 20, 25, 35, 50],
        fill: true,
        backgroundColor: '#faa800',
        borderColor: '#faa800',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#faa800',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#faa800',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  new Chart($('#tile-chart-3'), {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [30, 22, 18, 25, 40, 55, 60],
        fill: false,
        backgroundColor: '#34a853',
        borderColor: '#34a853',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#34a853',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#34a853',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  new Chart($('#tile-chart-4'), {
    type: 'bar',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset',
        data: [50, 45, 30, 20, 25, 35, 50],
        fill: true,
        backgroundColor: '#e53935',
        borderColor: '#e53935',
        borderWidth: 2,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#e53935',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBackgroundColor: '#e53935',
        pointHoverBorderColor: '#fff',
        pointHoverBorderWidth: 2,
        pointRadius: [0, 4, 4, 4, 4, 4, 0],
        pointHitRadius: 10,
        spanGaps: false
      }]
    },
    options: {
      scales: {
        xAxes: [{
          display: false
        }],
        yAxes: [{
          display: false,
          ticks: {
            min: 0,
            max: 60
          }
        }]
      },
      legend: {
        display: false
      }
    }
  });

  // Tab chart
  Morris.Area({
    element: 'multiple',
    data: [{
      period: '2011',
      apple: 120,
      sony: 110,
      samsung: 40
    }, {
      period: '2012',
      apple: 180,
      sony: 130,
      samsung: 170
    }, {
      period: '2013',
      apple: 120,
      sony: 170,
      samsung: 100
    }, {
      period: '2014',
      apple: 90,
      sony: 130,
      samsung: 40
    }, {
      period: '2015',
      apple: 120,
      sony: 150,
      samsung: 70
    }, {
      period: '2016',
      apple: 60,
      sony: 70,
      samsung: 90
    }, {
      period: '2017',
      apple: 170,
      sony: 190,
      samsung: 140
    }],
    xkey: 'period',
    ykeys: ['apple', 'sony', 'samsung'],
    labels: ['Apple', 'Sony', 'Samsung'],
    pointSize: 3,
    fillOpacity: 0.1,
    pointStrokeColors: ['#1d87e4', '#34a853', '#faa800'],
    behaveLikeLine: true,
    gridLineColor: '#e0e0e0',
    lineWidth: 1,
    hideHover: 'auto',
    lineColors: ['#1d87e4', '#34a853', '#faa800'],
    resize: true
  });

  // Peity charts
  $('[data-chart="peity"]').each(function () {
    var type = $(this).attr('data-type');
    $(this).peity(type);
  });

  // Donut chart
  Morris.Donut({
    element: 'donut',
    data: [{
      label: "Android",
      value: 34

    }, {
      label: "iOS",
      value: 67
    }, {
      label: "Windows",
      value: 45
    }],
    resize: true,
    colors: ['#1d87e4', '#faa800', '#e53935']
  });

  // Vector map
  $('#vector-map').vectorMap({
    map: 'usa_en',
    backgroundColor: null,
    borderColor: null,
    borderOpacity: 0.5,
    borderWidth: 1,
    color: '#1d87e4',
    enableZoom: true,
    hoverColor: '#1d87e4',
    hoverOpacity: 0.7,
    normalizeFunction: 'linear',
    selectedColor: '#faa800',
    selectedRegions: ['fl', 'ca', 'tx', 'ny', 'nd', 'oh'],
    showTooltip: true
  });
})(jQuery);
